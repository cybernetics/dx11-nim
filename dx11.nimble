# Package

version       = "0.1.0"
author        = "Crunchy Space"
description   = "Session manager for x11docker"
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["dx11"]
binDir        = "build"


# Dependencies

requires "nim >= 1.0.0"
requires "commandeer >= 0.12.3"
