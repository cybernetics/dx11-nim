" Syntax Highlighting
:syn on

" Use 3 spaces per tab
:set tabstop=3

" Expand tabs into spaces
":set expandtab

" Highlight search occurrences
:set hlsearch

" Last status
:set ls=2

" Display the file name, line ending format,
" column, line, total lines and the current command
:set statusline=%f%=[%{&ff}]\ [C%v,L%l/%L]

:set cm=blowfish2
