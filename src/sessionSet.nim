import os
import strformat
import common
import config

proc setSession*(sessionName: string, useTor: bool): bool =
    try:
        if not isValidSessionName(sessionName):
            raise newException(Exception, "The session name must contain letters, numbers, dashes or dots, and start with a letter")

        let sessionPaths = getPathsForSession(sessionName)

        if not existsDir(sessionPaths.basePath) or not existsFile(
                sessionPaths.configFile):
            raise newException(OSError, "There is no such session: " & sessionName)

        ensureSessionPaths(sessionPaths)

        # Read/update config
        var config = readSessionConfig(sessionName)

        # encryption status can't be changed

        # Update Tor settings only
        config.useTor = useTor

        echo fmt"Using Tor: {useTor}"

        # Store settings
        writeSessionConfig(sessionName, config)
        echo "Done"
        return true

    except OSError:
        echo getCurrentExceptionMsg()
        return false
